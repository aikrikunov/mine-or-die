class = require 'lib.middleclass'

require "config"
require "world"
require "player"
require "field"
require "gui"

score1 = 0
score2 = 0
isGameOver = false

function love.load()
  love.window.setFullscreen(true, "desktop")
  world = World:new()
  gui = Gui:new()
end

function love.update(dt)
  if not isGameOver then
    world:update(dt)
    world:checkHoldKeys(dt)
  end
end

function love.keyreleased(key, code)
  if key == "escape" then
    love.event.quit()
  end
  if not isGameOver then
    world:checkHitKeys(key, code)
  end
end

function love.draw()
  if not isGameOver then
    world:draw()
  end
  gui:draw()
end

function gameover(winner)
  if     winner == world.players[1] then
    score1 = score1 + 1
  elseif winner == world.players[2] then
    score2 = score2 + 1
  end

  if score1 >= 3 or score2 >= 3 then
    isGameOver = true
  else
    world = World:new()
  end
end