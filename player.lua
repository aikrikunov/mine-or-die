Player = class('Player')

function Player:initialize(x, y, color)
  self.x = x
  self.y = y
  self.dx = 0
  self.dy = 0
  self.w = 32
  self.h = 32
  self.color = color
  self.field = nil
  self.energy = 0
end

function Player:draw()
  love.graphics.setColor(self.color.r, self.color.g, self.color.b)
  love.graphics.circle('fill', self.x+self.w/2, self.y+self.h/2, self.w/2)
  love.graphics.setColor(0, 0, 0)
end

function Player:setMomentum(dir)
  if     dir == 'u' then self.dy = -PLAYER_SPEED
  elseif dir == 'd' then self.dy =  PLAYER_SPEED
  elseif dir == 'l' then self.dx = -PLAYER_SPEED
  elseif dir == 'r' then self.dx =  PLAYER_SPEED end
end

function Player:move(x,y)
  self.x = x
  self.y = y
  self.dx = 0
  self.dy = 0
end

function Player:mine(field)
  self.energy = self.energy + 1
end

function Player:attack()
  self.energy = self.energy * ATTACK_COST
end
