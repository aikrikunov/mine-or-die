# Правила игры

Игроки размещаются на поле. На поле случайным образом появляются источники энергии. Чтобы собрать энергию игрокам нужно коснуться источника. Задача игроков убить друг друга. В бою побеждает игрок у которого больше энергии. Если у агрессора меньше энергии, чем у жертвы, агрессор теряет часть энергии. Побеждает игрок который первым убьет соперника 3 раза.

# Управление

Действие | Игрок 1 | Игрок 2
---------|---------|--------
Движение |WASD     |OKL;
Атака    |E        |P