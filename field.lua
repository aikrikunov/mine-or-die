Field = class('Field')

function Field:initialize(x, y)
  self.x = x
  self.y = y
  self.w = 32
  self.h = 32
end

function Field:draw()
  love.graphics.setColor(0, 0, 255)
  love.graphics.rectangle('fill', self.x, self.y, self.w, self.h )
  love.graphics.setColor(0, 0, 0)
end

function Field:mine(player)
  selself.player = player
end

function Field:unmine()
  selself.player = nil
end