Gui = class('Gui')

function Gui:initialize()
end

function Gui:draw() 
  if not isGameOver then
    local p1, p2 = world.players[1], world.players[2]
    love.graphics.setColor(255, 255, 255)
    love.graphics.setFont(love.graphics.newFont(24))
    love.graphics.print(math.floor(p1.energy), 16, world.h-32)
    love.graphics.print(score1 .. " : " .. score2, world.w/2, world.h-32)
    love.graphics.print(math.floor(p2.energy), world.w-64, world.h-32)
    love.graphics.setColor(0, 0, 0)
  else
    local winner = 0
    if score1 >= 3 then
      winner = 1
    elseif score2 >= 3 then
      winner = 2
    end
    love.graphics.setColor(255, 255, 255)
    love.graphics.setFont(love.graphics.newFont(36))
    love.graphics.print("PLAYER " .. winner .. " WON", 16, 16)
  end
end