World = class('World')

function World:initialize()
  self.x = 0
  self.y = 0

  self.w = love.graphics.getWidth()
  self.h = love.graphics.getHeight()
  self.players = {
      Player:new(0,0, {r=255,g=0,b=0} ),
      Player:new(self.w-32,0, {r=0,g=255,b=0} )
  }
  self.fields = {
      Field:new(self.w*1/4,self.h/2),
      Field:new(self.w*3/4,self.h/2)
  }

  self.score1 = 0
  self.score2 = 0
end

function World:update(dt)
  -- move players
  for _,p in ipairs(self.players) do
    local dx, dy = p.dx, p.dy
    local x, y = p.x+p.dx*dt, p.y+p.dy*dt
    if not (x > self.x and x < self.w - p.w) then dx = 0 end
    if not (y > self.y and y < self.h - p.h) then dy = 0 end

    p:move(p.x + dx * dt, p.y + dy * dt)

    -- check collisions with
    for fi,f in ipairs(self.fields) do
      if areCollide(p, f) then
        -- mine and remove field
        p:mine()
        table.remove(self.fields, fi)

        -- spawn new field
        if f.x < self.w/2 then
          repeat
            local angle = math.random()*math.pi*2
            x = math.cos(angle)*SPAWN_RANGE + f.x + f.w/2
            y = math.sin(angle)*SPAWN_RANGE + f.y + f.h/2
          until x > 0 and x < self.w/2-f.w and y > 0 and y < self.h-f.h
        else
          repeat
            local angle = math.random()*math.pi*2
            x = math.cos(angle)*SPAWN_RANGE + f.x
            y = math.sin(angle)*SPAWN_RANGE + f.y
          until x > self.w/2 and x < self.w-f.w and y > 0 and y < self.h-f.h
        end
        table.insert(self.fields, Field:new(x,y))
      end
    end
  end
end

function World:draw()
  for _,p in ipairs(self.players) do p:draw() end
  for _,f in ipairs(self.fields) do f:draw() end
end

-- actions on continious key press
function World:checkHoldKeys(dt)
  local p1 = self.players[1]
  local p2 = self.players[2]

  -- move
  if love.keyboard.isDown('w') then
    self:move(p1, 'u')
  elseif love.keyboard.isDown('s') then
    self:move(p1, 'd')
  end
  if love.keyboard.isDown('a') then
    self:move(p1, 'l')
  elseif love.keyboard.isDown('d') then
    self:move(p1, 'r')
  end

  if love.keyboard.isDown('o') then
    self:move(p2, 'u')
  elseif love.keyboard.isDown('l') then
    self:move(p2, 'd')
  end
  if love.keyboard.isDown('k') then
    self:move(p2, 'l')
  elseif love.keyboard.isDown(';') then
    self:move(p2, 'r')
  end
end

-- actions on one time press
function World:checkHitKeys(key, code)
  local p1 = self.players[1]
  local p2 = self.players[2]

  -- attacking
  if key == 'e' then
    self:attack(p1, p2)
  end
  if key == 'p' then
    self:attack(p2, p1)
  end
end

-- LOCATION ACTIONS E.G.MESSAGES
function World:move(player, dir)
  if     dir == 'u' then player:setMomentum('u')
  elseif dir == 'd' then player:setMomentum('d')
  elseif dir == 'l' then player:setMomentum('l')
  elseif dir == 'r' then player:setMomentum('r') end
end

function World:attack(attacker, target)
  local distance = math.sqrt(
      math.abs(target.x+target.w/2-attacker.x-attacker.w/2)^2
      +
      math.abs(target.y+target.h/2-attacker.y-attacker.h/2)^2
  )
  if distance <= ATTACK_RANGE then
    if math.floor(attacker.energy) > math.floor(target.energy) then
      gameover(attacker)
    else
      attacker:attack()
    end
  end
end

function areCollide(o1, o2)
  return
      o1.x < o2.x+o2.w and
      o2.x < o1.x+o1.w and
      o1.y < o2.y+o2.h and
      o2.y < o1.y+o1.h
end